package com.example.s04proj03;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get linear layout in which we will be dynamically adding the CuisineListFragments (one for each cuisine)
        LinearLayout linearLayout = findViewById(R.id.linear_layout);

        // FragmentManager is the mechanism for adding fragments programmatically
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        // Get singleton instance of menu
        HashMap<String, ArrayList<Dish>> dishesByCuisine = Menu.getInstance().dishesByCuisine();

        // Loop through all cuisines (dish categories)
        for (Map.Entry<String, ArrayList<Dish>> entry : dishesByCuisine.entrySet()) {
            // Get cuisine enum for this cuisines (dish category)
            Cuisine cuisine = Cuisine.valueOf(entry.getKey());

            // Create a corresponding CuisineListFragment for this cuisine object
            Fragment fragment = CuisineListFragment.newInstance(cuisine);

            // Add the CuisineListFragment to the linear layout set up in our XML layout file
            fragmentTransaction.add(linearLayout.getId(), fragment , null);
        }
        fragmentTransaction.commit();
    }
}